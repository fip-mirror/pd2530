module faultctl(
	input dogfood,
	input gang_fault_in,
	input cmd_fault_in,
	input parity_fault_in,
	input fault_clear,

	output reg gang_fault_out,
	output reg cmd_fault_out,
	output reg parity_fault_out,
	output reg wdt_fault_out,

	input wdt_timer,
	input clk
);

reg dogfood_delay1;
reg dogfood_delay2;
wire wdt;
wire fault_clear_checked;

timeout wdt_timeout(
	.clk(clk),
	.signal(dogfood_delay1 ^ dogfood_delay2),
	.clear (dogfood_delay1 ^ dogfood_delay2),
	.timer (wdt_timer),
	.timedout(wdt)
);

always @(posedge clk) begin
	dogfood_delay1 <= dogfood;
	dogfood_delay2 <= dogfood_delay1;
end

assign fault_clear_checked = fault_clear &
	!(gang_fault_in | cmd_fault_in | parity_fault_in | wdt);

always @(posedge clk) begin
	if (gang_fault_in) begin
		gang_fault_out <= 1'b1;
	end else if (fault_clear_checked) begin
		gang_fault_out <= 1'b0;
	end
	if (cmd_fault_in) begin
		cmd_fault_out <= 1'b1;
	end else if (fault_clear_checked) begin
		cmd_fault_out <= 1'b0;
	end
	if (parity_fault_in) begin
		parity_fault_out <= 1'b1;
	end else if (fault_clear_checked) begin
		parity_fault_out <= 1'b0;
	end
	if (wdt) begin
		wdt_fault_out <= 1'b1;
	end else if (fault_clear_checked) begin
		wdt_fault_out <= 1'b0;
	end
end

endmodule
