# BOM: PD2530-Transformer

## Parts

| Qty | Item | Manufacturer | Part number | Substitutions? |
|----:|------|--------------|-------------|----------------|
|   1 | Transformer, 300VA 2x30V | Hammond | 1182P30 | With care |
|   8 | 4.75mm quick connect terminals | TE Connectivity | 61945-1 | Y |
|   8 | Heatshrink sleeves, 20mm long, diameter suitable for the QC connectors | Generic | N/A | Y |

## Assembly

1. Slide one heatshrink sleeve onto each transformer wire.
2. Crimp one terminal onto each transformer wire.
3. Shrink the heatshrink sleeves over the terminals.
