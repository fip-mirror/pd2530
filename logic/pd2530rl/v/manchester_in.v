// Simple Manchester decoder. Assumes that the encoded clock is about 1/8 clk.
module manchester_in(
	input manch,
	input manch_timer,

	output manch_clk,
	output signal,
	output detected,

	input clk
);

reg data = 1'b0;
reg [2:0] count = 3'b0;
reg delayed_manch;

always @(posedge clk) begin
	if (manch_clk | (count != 0)) begin
		count <= count + 1;
		if (count == 5) begin
			data <= manch;
			count <= 0;
		end
	end

	delayed_manch <= manch;
end

assign manch_clk = manch ^ data;
assign signal    = data;

wire   timedout;

timeout detector(
	.clk(clk),
	.signal(delayed_manch ^ manch),
	.clear (delayed_manch ^ manch),
	.timer (manch_timer),
	.timedout(timedout)
);
assign detected = ~timedout;

endmodule

