// Timeout module. Accepts a timer pulse and a signal; if the signal does not
// assert between two timer pulses then a timeout is triggered. The signal
// itself may be routed to the .clear input as well in order to clear the
// timeout as soon as the signal returns.

module timeout (
	input clk,
	input signal,
	input clear,
	input timer,

	output reg timedout
);

reg saw_signal;

initial begin
	saw_signal <= 1'b0;
	timedout <= 1'b0;
end

always @(posedge clk)
begin
	if (signal)
	begin
		saw_signal <= 1'b1;
	end
	else if (timer)
	begin
		saw_signal <= 1'b0;
	end

	if (timer & ~clear & ~saw_signal)
	begin
		timedout <= 1'b1;
	end
	else if (clear)
	begin
		timedout <= 1'b0;
	end
end

endmodule
