# BOM: PD2530-RegulatorAssembly

## Parts

| Qty | Item | Manufacturer | Part number | Substitutions? |
|----:|------|--------------|-------------|----------------|
|   1 | [PD2530-HeatsinkAssembly ] | Custom | N/A | N |

## Assembly

[PD2530-HeatsinkAssembly]: PD2530-HeatsinkAssembly.md
