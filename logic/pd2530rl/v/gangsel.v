module gangsel(
	input gang1_set,
	input gang1_reset,
	input gang1_fault,

	input gang2_set,
	input gang2_reset,
	input gang2_fault,

	input gang1_en,
	input gang2_en,
	input start,
	input stop,

	output reg presync_cmd,
	output reg gang_fault,

	input clk
);

initial begin
	presync_cmd <= 1'b0;
	gang_fault  <= 1'b0;
end

always @(posedge clk) begin
	if (gang1_en & gang2_en) begin
		gang_fault  <= 1'b1;
		presync_cmd <= 1'b0;
	end
	else if (gang1_en) begin
		gang_fault <= gang1_fault | (gang1_set & gang1_reset);
		if (gang1_set) begin
			presync_cmd <= 1'b1;
		end else if (gang1_reset) begin
			presync_cmd <= 1'b0;
		end
	end
	else if (gang2_en) begin
		gang_fault <= gang2_fault | (gang2_set & gang2_reset);
		if (gang2_set) begin
			presync_cmd <= 1'b1;
		end else if (gang2_reset) begin
			presync_cmd <= 1'b0;
		end
	end
	else begin
		gang_fault <= 1'b0;
		if (start & ~stop) begin
			presync_cmd <= 1'b1;
		end else if (stop & ~start) begin
			presync_cmd <= 1'b0;
		end
	end
end

endmodule
