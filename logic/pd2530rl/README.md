# PD2530RL

PD2530RL ("Regulator Logic") is the custom logic circuit for the PD2530. This
circuit, built in a small FPGA, sequences all on/off and setpoint selection
operations for the PD2530 regulator.

![Block diagram](pd2530rl.svg)

## Signals and pins

|        Pin | Mode | Signal        |
|-----------:|:----:|---------------|
|         12 |  O   | CIPO          |
|         13 |  I   | COPI          |
|         14 |  IP  | nCS           |
|         15 |  I   | SCK           |
|         19 |  I   | CLK           |
|         20 |  I   | LocalOsc      |
|       2, 1 |  IP  | nGang1+, -    |
|       7, 8 |  IP  | nGang2+, -    |
|       6, 5 |  ID  | OscIn+, -     |
|     30, 29 |  OD  | OscOut+, -    |
|         27 |  O   | SourceEnable  |
|         18 |  O   | CvEnable      |
|         32 |  O   | SinkInhibit   |
|         31 |  O   | SetpointSel   |
|         25 |  P   | Vcore (1.2V)  |
|   4, 9, 16 |  P   | Vcc (3.3V)    |
| 17, 24, 28 |  P   | Vcc (3.3V)    |
|  3, 21, 33 |  P   | GND           |

**O** = output, **OD** = output (differential), **I** = input, **IP** =
input (pullup), **ID** = input (differential), **P** = power

| Signal        | Description                                             |
|---------------|---------------------------------------------------------|
| CIPO          | SPI output. Shared with the FPGA config interface.      |
| COPI          | SPI input. Shared with the FPGA config interface.       |
| nCS           | SPI chip select. Shared with the FPGA config interface. |
| SCK           | SPI clock. Shared with the FPGA config interface.       |
| CLK           | System clock input. Nominal clock is 36 MHz.            |
| LocalOsc      | Local setpoint oscillator signal.                       |
| nGangN+, -    | Gang trigger N input.                                   |
| OscIn+, -     | External setpoint oscillator input, Manchester-coded.   |
| OutOut+, -    | External setpoint oscillator output, Manchester-coded.  |
| SourceEnable  | Regulator source enable signal, active high.            |
| CvEnable      | Constant voltage enable signal, active high.            |
| SinkInhibit   | Regulator sink inhibit signal, active high.             |
| SetpointSel   | Setpoint selector signal, active high.                  |

## Control interface

The control interface is a 16-bit SPI mode 0 register, with data transmitted
in full big endian (MSb-first, MSB-first). Maximum SPI clock is 4 MHz.

## Control register

| Bit  | Name           | Description                                               |
|------|----------------|-----------------------------------------------------------|
| 0001 | `C.OscOut`     | Manchester-coded oscillator output is enabled             |
| 0002 | `C.OscChain`   | Manchester output duplicates Manchester input             |
| 0004 | `C.OscExt`     | External (Manchester) oscillator is selected              |
| 0010 | `C.SyncToOsc`  | Supply startup is synchronized to oscillator falling edge |
| 0020 | `C.Gang1En`    | Gang trigger 1 is active                                  |
| 0040 | `C.Gang2En`    | Gang trigger 2 is active                                  |
| 0100 | `C.Start`      | Local start command                                       |
| 0200 | `C.Stop`       | Local stop command                                        |
| 0400 | `C.SrcEn`      | If the supply is up, source is enabled                    |
| 0800 | `C.SnkEn`      | If the supply is up, sink is enabled                      |
| 1000 | `C.PreregEn`   | Preregulator is enabled                                   |
| 4000 | `C.FaultClear` | Clear a fault condition                                   |
| 8000 | `C.Parity`     | Odd parity bit                                            |

All unspecified bits are reserved, and must be set to zero. The parity bit
`C.Parity` is calculated over all bits including reserved bits, and should be
set to produce an odd number of set bits in the register.

## Status register

| Bit  | Name              | Description                                                                   |
|------|-------------------|-------------------------------------------------------------------------------|
| 0001 | `S.ExtOscPresent` | External (Manchester) oscillator input is detected                            |
| 0002 | `S.SupplyCmd`     | The supply is currently commanded up                                          |
| 0004 | `S.PresyncCmd`    | The supply is currently commanded up, pending synchronization with oscillator |
| 0100 | `S.WdtFail`       | Watchdog timer has tripped                                                    |
| 0200 | `S.CfgError`      | An invalid combination of settings was loaded into the control register       |
| 0400 | `S.GangError`     | An invalid condition exists on a selected gang interface                      |
| 0800 | `S.ParityError`   | An invalid parity bit was received at the control register                    |
| 4000 | `S.Fault`         | Any fault flag is asserted                                                    |
| 8000 | `S.Parity`        | Odd parity bit                                                               |

All unspecified bits are reserved, and will be set to zero. The parity bit
`S.Parity` is calculated over all bits including reserved bits, and will be
set to produce an odd number of set bits in the register.

Invalid parity implies a communications error between the CPU and PD2530RL;
the most reliable way to deactivate the supply in this case is to first attempt
to transmit a disabling command to the control register (e.g. `C.Stop` asserted
and all `C.*En` bits deasserted), and then cease transmissions to allow the
watchdog to time out.

## Clocks and timeouts

The system clock runs at a nominal 36 MHz, and several slower pulse signals
are generated from this:

- CLK / 8 = 4.5 MHz: Manchester output clock
- CLK / 131072 = 275 Hz: watchdog timeout clock and Manchester detection timer
- CLK / 524288 = 68.7 Hz: gang trigger timeout clock

## Fault detection

The PD2530RL includes several fault detectors designed to trip in any
invalid condition. When tripped, `PreregEnable` is forced low and both
`SourceInhibit` and `SinkInhibit` are forced high, disabling the regulator. The
`S.Fault` bit and a specific bit indicating the type of fault are both set.

When a fault is detected, it is latched until the `C.FaultClear` bit is set.
To discourage setting this bit on every operation, simultaneous operation of
`C.FaultClear` and any control "enable" signal will trigger a command error.

### Watchdog

The PD2530RL includes a watchdog timer, fed by rising edges on `nCS`. If not
fed, the watchdog will expire between 1 and 2 cycles of the 275 Hz watchdog
timeout clock. When the watchdog expires, the `S.WdtFail` fault signal is
asserted until the next time the `C.FaultClear` bit is written.

### Invalid command

Any invalid combination of command bits will trigger a fault with `S.CfgError`
set. This includes the following:

- Both `C.Gang1En` and `C.Gang2En` set
- Operation of `Start` and `Stop` simultaneously
- External oscillator is selected, oscillator is enabled, but the Manchester
  decoder detects no signal
- Any `C.*En` enable signal is asserted simultaneously with `C.FaultClear`.

### Gang error

An invalid state on a selected gang interface will trigger a fault with
`S.GangError` set. Disabled gang interfaces will not trigger faults. Invalid
states include:

- Invalid `00` (both lines asserted) symbol
- Assertion of a non-idle (idle = `11`) symbol for between 14ms and 29ms
  (as determined by the gang trigger timeout clock): "stuck gang"

The invalid symbol should be impossible when using the recommended input
circuit (dual optocouplers in antiparallel), so this error may be reported
as "stuck gang".

### Parity error

The control register contains an odd parity bit that must be set correctly.
Writing the register with an incorrect parity bit will trigger a fault with
`S.ParityError` set.

## External oscillator

The PD2530RL can transmit the setpoint oscillation signal, normally supplied
by the CPU, over a Manchester-coded link to another PD2530RL. This allows
multiple regulator channels to oscillate in phase with each other, with the
signal being transmitted over an isolating transformer.

The Manchester output can be enabled via the `C.OscOut` bit. The `C.OscChain`
bit causes the Manchester-coded input to be immediately redriven out the output,
allowing more than 2 regulators to share one signal in a chain. If this bit
is clear, the primary oscillation signal from `LocalOsc` is encoded and
transmitted.

The presence or absence of a Manchester signal can be monitored via the
`S.ExtOscPresent` bit, which will assert on any Manchester input edge and
deassert on timeout (900 – 1800 µs).

Normally, the setpoint is chosen by the `C.Setpt` bit directly, but the
oscillator is used instead by asserting `C.OscEn`. The source can then be
selected as either `LocalOsc` or the external oscillator by deasserting or
asserting `C.OscExt`.

## Local control

The `C.Start` and `C.Stop` bits can be used to request that the supplies start
immediately. These bits are always active, even when a gang is enabled. Note
that asserting both simultaneously will stop the supply and trigger a fault.

These bits affect the main supply flipflop, which sits _before_ the oscillator
synchronizer, so if immediate startup is required, the oscillator synchronizer
(`C.SyncToOsc`) must be disabled.

## Ganged control

To allow simultaneous startup of multiple supplies, a gang trigger can be
fed to all supplies, with enganged supplies having their `C.Gang1En` or
`C.Gang2En` bits asserted.

Gang signals are active low; assertion of the positive signal will trigger the
supply to start, and assertion of the negative signal will trigger the supply
to stop. A signal that is stuck asserted for 14ms to 29ms, or an invalid state
with both signals asserted, will cause a gang fault and shut off the supply.

## Oscillator synchronized startup

When using oscillating mode, it is desirable to have the power supplies start
up in sync with the oscillator's zero phase point. To support this, the
PD2530RL contains a synchronizer that can be selected by asserting
`C.SyncToOsc`. The synchronizer delays the rising edge of the supply command
until the next falling edge of the oscillator signal, but allows the falling
edge of the supply command to pass immediately.

If the oscillator is set to a long period (about half a second or longer), it
is recommended to perform an additional procedure in software to ensure
startup is not delayed too long:

```
wait until S.PresyncCmd;

let osc_phase := phase of the oscillator in seconds, 0 = falling edge
if (osc_phase < -0.1s || osc_phase > 0.1s)
{
    # Timeout is a long way off.
    reset_osc();
}
if (osc_phase > -0.1s && osc_phase < 0.1s)
{
    # Near timeout. To avoid a race, delay at least 0.2s and check again
    delay(0.2s);
    if (!S.SupplyCmd) {
        reset_osc();
    }
    # else we were just before the edge and the supply already triggered
}
```

## Building the FPGA bitstream

Just type `make`. IceStorm is required, and can be downloaded from:
https://github.com/FPGAwars/tools-oss-cad-suite
