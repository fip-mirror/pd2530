#!/bin/sh

kicad_export -O export --clear \
	--schem \
	--f-fab \
	--ibom --ibom-exclude-not-in-bom \
	--inventree \
	--markdown-bom \
	--gerbers --gerb-n-layers=1 --gerb-pack --gerb-render \
	pd2530-heatsink-assy

