module gangdecode_tb();

reg clk, ngangp, ngangn, ngangp_lat, ngangn_lat, timer;
wire cmd_set, cmd_reset, fault;

gangdecode gangdecode(
	.clk(clk),
	.ngangp(ngangp_lat), .ngangn(ngangn_lat),
	.timer(timer),
	.cmd_set(cmd_set), .cmd_reset(cmd_reset),
	.fault(fault)
);

always @(posedge clk) begin
	ngangp_lat <= ngangp;
	ngangn_lat <= ngangn;
end

initial begin
	$dumpfile("out.vcd");
	$dumpvars(0,gangdecode_tb);

	// Get started
		clk = 1'b0; ngangp = 1'b1; ngangn = 1'b1; timer = 1'b0;
	#1	clk = 1'b1;
	#1	assert(~cmd_set);
		assert(~cmd_reset);
		assert(~fault);

	// Normal operations
		clk = 1'b0; ngangp = 1'b0;
	#1	clk = 1'b1;
	#1	assert(cmd_set);
		assert(~cmd_reset);
		assert(~fault);

		clk = 1'b0; ngangp = 1'b1; ngangn = 1'b0;
	#1	clk = 1'b1;
	#1	assert(~cmd_set);
		assert(cmd_reset);
		assert(~fault);

	// Immediate fault on both together
		clk = 1'b0; ngangp = 1'b0; ngangn = 1'b0;
	#1	clk = 1'b1;
	#1	assert(~cmd_set);
		assert(~cmd_reset);
		assert(fault);

	// Back to idle
		clk = 1'b0; ngangp = 1'b1; ngangn = 1'b1;
	#1	clk = 1'b1;
	#1	assert(~cmd_set);
		assert(~cmd_reset);
		assert(~fault);

	// Time out
		clk = 1'b0; ngangp = 1'b0; ngangn = 1'b1;
	#1	clk = 1'b1;
	#1	assert(cmd_set);
		assert(~cmd_reset);
		assert(~fault);
	#1	clk = 1'b0; timer = 1'b1;
	#1	clk = 1'b1;
	#1	clk = 1'b0;
	#1	clk = 1'b1;
	#1	assert(~cmd_set);
		assert(~cmd_reset);
		assert(fault);
end

endmodule
