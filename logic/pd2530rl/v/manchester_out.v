module manchester_out(
	input signal,
	input manch_clk,

	output reg manch,

	input clk
);

always @(posedge clk) begin
	manch <= manch_clk ^ signal;
end

endmodule
