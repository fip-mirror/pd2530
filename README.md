# FIP PD2530

**PRERELEASE**, this project is not complete.

PD2530 (Power supply, Dual, 25V, 3.0A) is the flagship power supply in the Free
Instruments Project's [PDP1](https://gitea.alexisvl.rocks/fip/pdp1) series. Its
features include:

- Output up to 25V at 3A
- Two-quadrant (source and sink) operation including downprogramming
- Oscillation between two setpoints
- Internal parallel and series operation
- Ganged operation via PDP1 link cable
- Synchronized startup and oscillation in parallel and series mode
- 10mV, 1mA resolution from 10mV to 25V and 5mA to 3A
- 0.1% accuracy once calibrated
- 150W hybrid-linear in a small package: 250mm x 190mm x 110mm
- All the specifications of the PDP1 control module:
  - Separate mechanical knobs for voltage and current on each channel
  - Always-up voltage and current displays
  - SCPI over Ethernet, USB, and RS-232

## Building

A PD2530 can be built by starting from the [root bill of materials](BOM/README.md).

## License

PD2530 and the PDP1 family are public-domain.
