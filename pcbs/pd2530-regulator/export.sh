#!/bin/sh

kicad_export -O export --clear \
	--schem \
	--f-fab --b-fab \
	--ibom --ibom-exclude-not-in-bom --ibom-jlcless \
	--inventree --inventree-jlcless \
	--markdown-bom \
	--gerbers --gerb-n-layers=6 --gerb-pack --gerb-render \
	pd2530-regulator


