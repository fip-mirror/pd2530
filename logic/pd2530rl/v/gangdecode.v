module gangdecode (
	input clk,
	input ngangp, ngangn,
	input timer,

	output cmd_set,
	output cmd_reset,
	output fault
);

reg last_ngangp, last_ngangn;
wire gang_cmd_set, gang_cmd_reset, gang_timedout, gang_idle, gang_invalid;

assign gang_cmd_set   = ~ngangp & ngangn & last_ngangp;
assign gang_cmd_reset = ~ngangn & ngangp & last_ngangn;
assign gang_idle      = ngangp & ngangn;
assign gang_invalid   = ~ngangp & ~ngangn;

assign fault = gang_invalid | gang_timedout;

assign cmd_set   = gang_cmd_set   & ~fault;
assign cmd_reset = gang_cmd_reset & ~fault;

timeout gang_timeout(
	.clk(clk),
	.signal(ngangp & ngangn),
	.clear (ngangp & ngangn),
	.timer (timer),
	.timedout(gang_timedout)
);

initial begin
	last_ngangp <= 1'b1;
	last_ngangn <= 1'b1;
end

always @(posedge clk)
begin
	last_ngangp <= ngangp;
	last_ngangn <= ngangn;
end

endmodule
