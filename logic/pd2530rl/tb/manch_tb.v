`define MANCH_T 8

module manch_tb();

reg  clk, manch_clk, manch_clk_en = 1'b1, data_in = 1'b0, manch_timer = 1'b0;
wire manch, manch_clk_recov, data_out, detected;

manchester_out manchester_out(
	.signal(data_in), .manch_clk(manch_clk & manch_clk_en),
	.manch(manch), .clk(clk)
);

manchester_in manchester_in(
	.manch(manch), .manch_timer(manch_timer),
	.manch_clk(manch_clk_recov), .signal(data_out),
	.detected(detected),
	.clk(clk)
);

initial begin
	$dumpfile("out.vcd");
	$dumpvars(0,manch_tb);

	clk = 1'b0;
	forever begin
		#1 clk = ~clk;
	end
end

initial begin
	manch_clk = 1'b0;
	forever begin
		#`MANCH_T manch_clk = ~manch_clk;
	end
end

initial begin
	integer i;
	for (i = 0; i < 32; i = i + 1) begin
		data_in = $urandom % 2;
		#`MANCH_T ;
		assert(data_out == data_in);
		#`MANCH_T ;
		assert(data_out == data_in);
		assert(detected);
	end

	// Test the timeout
	manch_clk_en = 1'b0;
	manch_timer = 1'b1;
	#`MANCH_T
	#`MANCH_T
	#`MANCH_T
	assert(~detected);

	$finish;
end

endmodule
