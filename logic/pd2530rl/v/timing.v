`define SLOW_BIT 19
`define FAST_BIT 17

module timing (
	input clk,
	output clk_manchester,
	output fast_timer,
	output slow_timer
);

reg [`SLOW_BIT:0] count = 19'b0;

reg last_countSlow;
reg last_countFast;

always @(posedge clk)
begin
	count <= count + 1;
	last_countSlow <= count[`SLOW_BIT];
	last_countFast <= count[`FAST_BIT];
end

assign clk_manchester = count[2];
assign fast_timer = count[`FAST_BIT] & ~last_countFast;
assign slow_timer = count[`SLOW_BIT] & ~last_countSlow;

endmodule
