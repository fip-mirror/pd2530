#!/bin/sh

kicad_export -O export --clear \
	--schem \
	--f-fab \
	--ibom \
	--inventree \
	--markdown-bom \
	--gerbers --gerb-n-layers=2 --gerb-pack --gerb-render \
	pd2530-panel
