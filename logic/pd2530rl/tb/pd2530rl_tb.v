`timescale 10ns/10ns
`define SCK_T 4

module pd2530rl_tb();

reg clk;
reg copi = 1'b0;
reg ncs  = 1'b1;
reg sck  = 1'b0;
reg localosc = 1'b0;
reg ngang1p  = 1'b1;
reg ngang1n  = 1'b1;
reg ngang2p  = 1'b1;
reg ngang2n  = 1'b1;
reg oscin    = 1'b0;

wire cipo;
wire oscoutp, oscoutn;
wire sourceenable;
wire cvenable;
wire sinkinhibit;
wire setpointsel;

`define C_OSCOUT        0
`define C_OSCCHAIN      1
`define C_OSCEXT        2
`define C_SYNCTOOSC     4
`define C_GANG1EN       5
`define C_GANG2EN       6
`define C_START         8
`define C_STOP          9
`define C_SRCEN        10
`define C_SNKEN        11
`define C_FAULTCLEAR   14

`define C_OSCOUT_b     15'b000000000000001
`define C_OSCCHAIN_b   15'b000000000000010
`define C_OSCEXT_b     15'b000000000000100
`define C_SYNCTOOSC_b  15'b000000000010000
`define C_GANG1EN_b    15'b000000000100000
`define C_GANG2EN_b    15'b000000001000000
`define C_START_b      15'b000000100000000
`define C_STOP_b       15'b000001000000000
`define C_SRCEN_b      15'b000010000000000
`define C_SNKEN_b      15'b000100000000000
`define C_FAULTCLEAR_b 15'b100000000000000

`define S_EXTOSCPRESENT 0
`define S_SUPPLYCMD     1
`define S_PRESYNCCMD    2
`define S_WDTFAIL       8
`define S_CFGERROR      9
`define S_GANGERROR    10
`define S_PARITYERROR  11
`define S_FAULT        14

pd2530rl pd2530rl(
	.clk(clk),
	.Cipo(cipo),
	.Copi(copi),
	.nCs(ncs),
	.Sck(sck),
	.LocalOsc(localosc),
	.nGang1p(ngang1p),
	.nGang1n(ngang1n),
	.nGang2p(ngang2p),
	.nGang2n(ngang2n),
	.OscIn(oscin),
	.OscOutp(oscoutp),
	.OscOutn(oscoutn),
	.SourceEnable(sourceenable),
	.CvEnable(cvenable),
	.SinkInhibit(sinkinhibit),
	.SetpointSel(setpointsel)
);

task shift(input [15:0] data, output [15:0] receive, output pval); begin
	integer i;
	for (i = 15; i >= 0; i -= 1) begin
		sck = 1'b0; copi = data[i];
		#`SCK_T sck = 1'b1;
		#`SCK_T receive[i] = cipo;
	end
	sck = 1'b0;
	#`SCK_T ;
	pval = ^receive;
end
endtask

task shift_par(input [14:0] data, output [15:0] receive, output pval); begin
	logic [15:0] data_p;
	data_p[15] = ~^data;
	data_p[14:0] = data;
	shift(data_p, receive, pval);
end
endtask

task spi_tx(input [14:0] data, output [14:0] receive); begin
	logic pval;
	logic [15:0] full_rx;
	ncs = 1'b0;
	#`SCK_T shift_par(data, full_rx, pval);
	#`SCK_T ncs = 1'b1;
	#`SCK_T
	#`SCK_T
	receive = full_rx[14:0];
end
endtask

initial begin
	$dumpfile("out.vcd");
	$dumpvars(0,pd2530rl_tb);

	clk = 1'b0;
	forever begin
		#1 clk = ~clk;
	end
end

reg [14:0] receive;

initial begin
	#20;
	assert(~sourceenable);
	assert(~cvenable);
	assert(sinkinhibit);

	// Clear any faults first
	spi_tx(`C_FAULTCLEAR_b, receive);
	spi_tx(`C_FAULTCLEAR_b, receive);
	assert(~receive[`S_FAULT]);

	// Try to turn the supply on
	#20;
	spi_tx(`C_SRCEN_b | `C_SNKEN_b | `C_START_b, receive);
	assert(sourceenable);
	assert(cvenable);
	assert(~sinkinhibit);

	// And off again
	#20;
	spi_tx(`C_SRCEN_b | `C_SNKEN_b | `C_STOP_b, receive);
	assert(~sourceenable);
	assert(~cvenable);
	assert(sinkinhibit);

	// Source only
	#20;
	spi_tx(`C_SRCEN_b | `C_START_b, receive);
	assert(sourceenable);
	assert(cvenable);
	assert(sinkinhibit);

	// Sink only
	#20;
	spi_tx(`C_SNKEN_b, receive);
	assert(~sourceenable);
	assert(cvenable);
	assert(~sinkinhibit);

	// And off again
	#20;
	spi_tx(`C_STOP_b, receive);
	assert(~sourceenable);
	assert(~cvenable);
	assert(sinkinhibit);

	// Try pulsing a gang input. Nothing should happen
	ngang1p = 1'b0;
	#4
	ngang1p = 1'b1;
	#4
	assert(~sourceenable & ~cvenable & sinkinhibit);

	// Turn on that gang input and now it should work.
	#20;
	spi_tx(`C_SRCEN_b | `C_SNKEN_b | `C_GANG1EN_b, receive);
	assert(~sourceenable & ~cvenable & sinkinhibit);
	ngang1p = 1'b0;
	#4
	ngang1p = 1'b1;
	#4
	assert(sourceenable & cvenable & ~sinkinhibit);

	// And back off.
	ngang1n = 1'b0;
	#4
	ngang1n = 1'b1;
	#4
	assert(~sourceenable & ~cvenable & sinkinhibit);

	// Repeat with the second gang input
	#20;
	spi_tx(`C_SRCEN_b | `C_SNKEN_b | `C_GANG2EN_b, receive);
	assert(~sourceenable & ~cvenable & sinkinhibit);
	ngang2p = 1'b0;
	#4
	ngang2p = 1'b1;
	#4
	assert(sourceenable & cvenable & ~sinkinhibit);

	// And back off.
	ngang2n = 1'b0;
	#4
	ngang2n = 1'b1;
	#4
	assert(~sourceenable & ~cvenable & sinkinhibit);

	// Local oscillator works
	#20;
	assert(~setpointsel);
	#4 localosc = 1'b1;
	#4 assert(setpointsel);
	#4 localosc = 1'b0;
	#4 assert(~setpointsel);

	// Sync to oscillator
	#20;
	localosc = 1'b1;
	spi_tx(`C_SRCEN_b | `C_STOP_b | `C_SYNCTOOSC_b, receive);
	assert(~sourceenable);
	spi_tx(`C_SRCEN_b | `C_START_b | `C_SYNCTOOSC_b, receive);
	#4 assert(~sourceenable);
	#4 localosc = 1'b0;
	#4 assert(sourceenable);
	spi_tx(`C_SRCEN_b | `C_STOP_b | `C_SYNCTOOSC_b, receive);
	#4 assert(~sourceenable);

	// Trigger a fault
	#20
	spi_tx(`C_SNKEN_b | `C_SRCEN_b | `C_STOP_b | `C_START_b, receive);
	spi_tx(`C_SNKEN_b | `C_SRCEN_b | `C_STOP_b | `C_START_b, receive);
	assert(receive[`S_FAULT] && receive[`S_CFGERROR]);
	assert(~sourceenable && ~cvenable && sinkinhibit);

	// Try to clear it while still commanding output
	spi_tx(`C_FAULTCLEAR_b | `C_SNKEN_b | `C_SRCEN_b | `C_START_b, receive);
	spi_tx(`C_FAULTCLEAR_b | `C_SNKEN_b | `C_SRCEN_b | `C_START_b, receive);
	assert(receive[`S_FAULT] && receive[`S_CFGERROR]);
	assert(~sourceenable && ~cvenable && sinkinhibit);

	// Clear it properly
	spi_tx(`C_FAULTCLEAR_b | `C_STOP_b, receive);
	spi_tx(`C_FAULTCLEAR_b | `C_STOP_b, receive);
	assert(~receive[`S_FAULT]);
	assert(~sourceenable && ~cvenable && sinkinhibit);

	#20;
	$finish;
end

endmodule
