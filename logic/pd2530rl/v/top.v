module top (
	input  Clki,
	output Cipo,
	input  Copi,
	input  nCs,
	input  Sck,
	input  LocalOsc,
	input  nGang1p, nGang1n,
	input  nGang2p, nGang2n,
	input  OscInp, //OscInn,
	output OscOutp, OscOutn,
	output SourceEnable,
	output CvEnable,
	output SinkInhibit,
	output SetpointSel,
	output PreregEnable
);

wire clk;
SB_GB clk_gb(
	.USER_SIGNAL_TO_GLOBAL_BUFFER(Clki),
	.GLOBAL_BUFFER_OUTPUT(clk)
);

// Synchronize and pullup the gang trigger inputs
wire nGang1p_data, nGang1n_data, nGang2p_data, nGang2n_data;

SB_IO #(.PIN_TYPE(6'b0000_00), .PULLUP(1'b1))
nGang1pConfig (.PACKAGE_PIN(nGang1p), .D_IN_0(nGang1p_data), .INPUT_CLK(clk));
SB_IO #(.PIN_TYPE(6'b0000_00), .PULLUP(1'b1))
nGang1nConfig (.PACKAGE_PIN(nGang1n), .D_IN_0(nGang1n_data), .INPUT_CLK(clk));
SB_IO #(.PIN_TYPE(6'b0000_00), .PULLUP(1'b1))
nGang2pConfig (.PACKAGE_PIN(nGang2p), .D_IN_0(nGang2p_data), .INPUT_CLK(clk));
SB_IO #(.PIN_TYPE(6'b0000_00), .PULLUP(1'b1))
nGang2nConfig (.PACKAGE_PIN(nGang2n), .D_IN_0(nGang2n_data), .INPUT_CLK(clk));

// Manchester I/O
wire OscIn;

SB_IO #(.PIN_TYPE(6'b0000_00), .IO_STANDARD("SB_LVDS_INPUT"))
OscIn_Config (.PACKAGE_PIN(OscInp), .D_IN_0(OscIn), .INPUT_CLK(clk));

pd2530rl pd2530rl(
	.clk(clk),
	.Cipo(Cipo),
	.Copi(Copi),
	.nCs(nCs),
	.Sck(Sck),
	.LocalOsc(LocalOsc),
	.nGang1p(nGang1p_data),
	.nGang1n(nGang1n_data),
	.nGang2p(nGang2p_data),
	.nGang2n(nGang2n_data),
	.OscIn(OscIn),
	.OscOutp(OscOutp),
	.OscOutn(OscOutn),
	.SourceEnable(SourceEnable),
	.CvEnable(CvEnable),
	.SinkInhibit(SinkInhibit),
	.SetpointSel(SetpointSel),
	.PreregEnable(PreregEnable)
);

endmodule
