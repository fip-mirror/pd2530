// Simple SPI peripheral module with a parity bit at the MSB.
//
// Fixed mode 0, big-endian. SCK must be at most 1/6 CLK.

module spi
#(
	parameter BITS=16
)
(
	input clk,
	input ncs,
	input sck,
	input copi,
	output cipo,

	input  [BITS-2:0] cipo_data,
	output [BITS-1:0] copi_data,
	output reg        parity_valid = 1'b1

);

reg [BITS-1:0] shift_cipo = 64'b0;
reg [BITS-1:0] shift_copi = 64'b0;
reg [BITS-1:0] out_reg = 64'b0;
reg ncs_sync = 1'b1, ncs_prev = 1'b1;
reg sck_sync = 1'b0, sck_prev = 1'b0;
reg copi_sync = 1'b0;

always @(posedge clk) begin
	ncs_sync <= ncs;
	ncs_prev <= ncs_sync;
	sck_sync <= sck;
	sck_prev <= sck_sync;
	copi_sync <= copi;

	if (~ncs_sync & ncs_prev) begin
		shift_cipo[BITS-2:0] <= cipo_data;
		shift_cipo[BITS-1] <= ~^cipo_data;
	end
	else if (ncs_sync & ~ncs_prev) begin
		out_reg <= shift_copi;
		parity_valid <= ^shift_copi;
	end
	else if (sck_sync & ~sck_prev) begin
		shift_copi[BITS-1:1] <= shift_copi[BITS-2:0];
		shift_copi[0] <= copi_sync;
	end
	else if (~sck_sync & sck_prev) begin
		shift_cipo[BITS-1:1] <= shift_cipo[BITS-2:0];
	end
end

assign cipo = ncs ? 1'bZ : shift_cipo[BITS-1];

assign copi_data = out_reg;

endmodule
