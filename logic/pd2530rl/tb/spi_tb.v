`timescale 10ns/10ns

// Test at 1/4 SCK - the spec is 1/6 SCK max, so we have some margin.
`define SCK_T 4

module spi_tb();

reg  clk, ncs = 1'b1, sck = 1'b0, copi = 1'b0;
wire cipo;

reg  [14:0] in_data;
wire [15:0] out_data;
wire        parity_valid;

spi #(.BITS(16)) spi (
	.clk(clk),
	.ncs(ncs),
	.sck(sck),
	.copi(copi),
	.cipo(cipo),
	.cipo_data(in_data),
	.copi_data(out_data),
	.parity_valid(parity_valid)
);

task shift(input [15:0] data, output [15:0] receive, output pval); begin
	integer i;
	for (i = 15; i >= 0; i -= 1) begin
		sck = 1'b0; copi = data[i];
		#`SCK_T sck = 1'b1;
		#`SCK_T receive[i] = cipo;
	end
	sck = 1'b0;
	#`SCK_T ;
	pval = ^receive;
end
endtask

task shift_par(input [14:0] data, output [15:0] receive, output pval); begin
	logic [15:0] data_p;
	data_p[15] = ~^data;
	data_p[14:0] = data;
	shift(data_p, receive, pval);
end
endtask

initial begin
	$dumpfile("out.vcd");
	$dumpvars(0,spi_tb);

	clk = 1'b0;
	forever begin
		#1 clk = ~clk;
	end
end

reg [15:0] receive;
reg pval;

initial begin
	#20 ;

	in_data = 15'h5A5A;
	#`SCK_T ncs = 1'b1;
	#`SCK_T ncs = 1'b0;
	shift_par(15'h1336, receive, pval);
	#`SCK_T ncs = 1'b1;
	#`SCK_T
	#`SCK_T

	assert(receive[14:0] == 15'h5A5A);
	assert(pval);
	assert(parity_valid);

	#20 ;

	#`SCK_T ncs = 1'b0;
	shift(16'h9336, receive, pval);
	#`SCK_T ncs = 1'b1;
	#`SCK_T
	#`SCK_T
	assert(receive[14:0] == 15'h5A5A);
	assert(~parity_valid);

	#100 ;
	$finish;
end

endmodule
