module pd2530rl (
	input  clk,
	output Cipo,
	input  Copi,
	input  nCs,
	input  Sck,
	input  LocalOsc,
	input  nGang1p, nGang1n,
	input  nGang2p, nGang2n,
	input  OscIn,
	output OscOutp, OscOutn,
	output SourceEnable,
	output CvEnable,
	output SinkInhibit,
	output SetpointSel,
	output PreregEnable
);

`define C_OSCOUT        0
`define C_OSCCHAIN      1
`define C_OSCEXT        2
`define C_SYNCTOOSC     4
`define C_GANG1EN       5
`define C_GANG2EN       6
`define C_START         8
`define C_STOP          9
`define C_SRCEN        10
`define C_SNKEN        11
`define C_PREREGEN     12
`define C_FAULTCLEAR   14

`define S_EXTOSCPRESENT 0
`define S_SUPPLYCMD     1
`define S_PRESYNCCMD    2
`define S_WDTFAIL       8
`define S_CFGERROR      9
`define S_GANGERROR    10
`define S_PARITYERROR  11
`define S_FAULT        14

// Timing generator

wire clk_manchester, fast_timer, slow_timer;
timing timing(
	.clk(clk), .clk_manchester(clk_manchester),
	.fast_timer(fast_timer), .slow_timer(slow_timer));

// SPI control
wire [14:0] status_reg;
wire [15:0] control_reg;
wire parity_valid;
spi #(.BITS(16)) spicontrol (
	.clk(clk),
	.ncs(nCs),
	.sck(Sck),
	.copi(Copi),
	.cipo(Cipo),
	.cipo_data(status_reg),
	.copi_data(control_reg),
	.parity_valid(parity_valid)
);
// Reserved status bits
assign status_reg[`S_WDTFAIL]       = wdt_fault_out;
assign status_reg[`S_CFGERROR]      = cmd_fault_out;
assign status_reg[`S_GANGERROR]     = gang_fault_out;
assign status_reg[`S_PARITYERROR]   = parity_fault_out;
assign status_reg[`S_FAULT]         = fault;
assign status_reg[7:3]              = 5'b0;
assign status_reg[13:12]            = 2'b0;

// Gang decoders
wire gang1_set, gang1_reset, gang1_fault, gang2_set, gang2_reset, gang2_fault;

gangdecode gd1(
	.clk(clk),
	.ngangp(nGang1p), .ngangn(nGang1n),
	.timer(slow_timer),
	.cmd_set(gang1_set), .cmd_reset(gang1_reset), .fault(gang1_fault));
gangdecode gd2(
	.clk(clk),
	.ngangp(nGang2p), .ngangn(nGang2n),
	.timer(slow_timer),
	.cmd_set(gang2_set), .cmd_reset(gang2_reset), .fault(gang2_fault));

// Control logic

wire presync_cmd;
wire gang_fault;
gangsel gangsel(
	.gang1_set(gang1_set), .gang1_reset(gang1_reset), .gang1_fault(gang1_fault),
	.gang2_set(gang2_set), .gang2_reset(gang2_reset), .gang2_fault(gang2_fault),
	.gang1_en(control_reg[`C_GANG1EN]), .gang2_en(control_reg[`C_GANG2EN]),
	.start(control_reg[`C_START]), .stop(control_reg[`C_STOP]),
	.presync_cmd(presync_cmd),
	.gang_fault(gang_fault),
	.clk(clk)
);

wire supply_cmd;
oscsync oscsync(
	.presync_cmd(presync_cmd), .osc(osc),
	.sync_to_osc(control_reg[`C_SYNCTOOSC]),
	.supply_cmd(supply_cmd),
	.clk(clk)
);

assign status_reg[`S_SUPPLYCMD]  = supply_cmd;
assign status_reg[`S_PRESYNCCMD] = presync_cmd;

// Fault management and watchdog
wire cmd_fault;
assign cmd_fault = (
	(control_reg[`C_GANG1EN] & control_reg[`C_GANG2EN]) |
	(control_reg[`C_START]   & control_reg[`C_STOP]) |
	(control_reg[`C_OSCEXT] & ~extosc_detected) |
	((control_reg[`C_SRCEN] | control_reg[`C_SNKEN]) & control_reg[`C_FAULTCLEAR])
);

wire fault, gang_fault_out, cmd_fault_out, parity_fault_out, wdt_fault_out;
faultctl faultctl(
	.dogfood(nCs),
	.gang_fault_in(gang_fault),
	.cmd_fault_in(cmd_fault),
	.parity_fault_in(~parity_valid),
	.fault_clear(control_reg[`C_FAULTCLEAR]),

	.gang_fault_out(gang_fault_out),
	.cmd_fault_out(cmd_fault_out),
	.parity_fault_out(parity_fault_out),
	.wdt_fault_out(wdt_fault_out),

	.wdt_timer(slow_timer),
	.clk(clk)
);

assign fault = gang_fault_out | cmd_fault_out | parity_fault_out | wdt_fault_out;

assign SourceEnable  =  (control_reg[`C_SRCEN] & supply_cmd & ~fault);
assign CvEnable      =  (control_reg[`C_SRCEN] | control_reg[`C_SNKEN])
                        & supply_cmd & ~fault;
assign SinkInhibit   = ~(control_reg[`C_SNKEN] & supply_cmd & ~fault);
assign PreregEnable  = control_reg[`C_PREREGEN] & ~fault;

// Manchester I/O

wire localosc_manch;
manchester_out manchester_out(
	.signal(LocalOsc),
	.manch_clk(clk_manchester),
	.manch(localosc_manch),
	.clk(clk)
);

wire selected_manch;
assign selected_manch = control_reg[`C_OSCCHAIN] ? OscIn : localosc_manch;
assign OscOutp = control_reg[`C_OSCOUT] ?  selected_manch : 1'bZ;
assign OscOutn = control_reg[`C_OSCOUT] ? ~selected_manch : 1'bZ;

wire extosc;
wire extosc_detected;
manchester_in manchester_in(
	.manch(OscIn), .manch_timer(fast_timer),
	.signal(extosc), .detected(extosc_detected),
	.clk(clk)
);
assign status_reg[`S_EXTOSCPRESENT] = extosc_detected;

wire osc;
assign osc = control_reg[`C_OSCEXT] ? extosc : LocalOsc; // TODO delay

wire spsel;
assign spsel = osc;
assign SetpointSel = spsel;

assign test34 = 1'b1;

endmodule
