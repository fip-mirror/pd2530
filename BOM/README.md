# Bills of Materials

This directory contains the bill(s) of materials for the PD2530 dual-channel
power supply. These bills of materials also contain links to build instructions.
For subassemblies, the build instructions are generally directly in the BOM.
To build a PD2530, start from the main [PD2530 Bill of Materials](PD2530.md).

## Materials tree

This is the full list of materials required in a hierarchical tree format. Note
that individual components on PCBAs are not listed here; PCBAs have their own
assembly data and are expected to be provided fully assembled to the build
process. (TODO: automate exporting the KiCad BOM data, and crossreferencing
Inventree part names with manuf/mpn)

- [PD2530]
  - [PD2530-Chassis]
  - [PD2530-Transformer]
    - Transformer, 300VA 2x30V (Hammond 1182P30)
    - 8x 4.75mm quick connect terminals
    - 8x heatshrink sleeves
  - [PD2530-FrontPanel]
  - [PD2530-RegulatorAssembly]
    - [PD2530-HeatsinkAssembly]
      - 100x69x36mm extruded aluminum heatsink (ASIN B07DH5T2RW)
      - 2x 100x25x10mm extruded aluminum heatsink (ASIN B00UJ95TOE)
      - 8x high-performance thermal pad (t-Global DC0011/05-TG-A482K-0.1-0)
      - [PD2530-HeatsinkAssemblyPCBA]
      - Mounting bracket (Keystone 4337)
      - 2x M4x10 Torx-drive zinc-plated screws
      - 6x M3x20 Torx-drive zinc-plated screws
      - 1x M3x6 Torx-drive zinc-plated screw
      - 6x #6 oversized (0.625in) washers
      - Medium-strength threadlocker (Loctite 242)
  - [PD2530-InternalCableSet]
  - 3x M3 nylon locknuts

## Tools

PD2530 is an involved project and generally assumes access to most of the tools
that would be available to the accomplished electronics hobbyist, perhaps at a
makerspace or similar. It's very possible that I've forgotten something here.

- PCB building:
  - SMT tweezers
  - Temperature-controlled soldering iron
  - Flush side-cutter
  - Note that no hot-air station is required, but you may find it helpful
  - Soldering microscope highly recommended
- Cable building:
  - Wire cutter
  - Wire stripper
  - Terminal crimper suitable for JST-XH, Molex KK 254, and 4.75mm Quick Connect
    terminals
  - Heat source for applying heatshrink (a pocket lighter will do)
- General:
  - Screwdriver set including hex bits
  - Drill and drill bits (inch sizes)
  - Metric tap set (M3 and M4 required)
  - Heat-set threaded insert tool (see [PD2530-Chassis] for a recommended kit
    including one)

[PD2530]: PD2530.md
[PD2530-Chassis]: PD2530-Chassis.md
[PD2530-Transformer]: PD2530-Transformer.md
[PD2530-FrontPanel]: PD2530-FrontPanel.md
[PD2530-RegulatorAssembly]: PD2530-RegulatorAssembly.md
[PD2530-InternalCableSet]: PD2530-InternalCableSet.md
[PD2530-HeatsinkAssembly]: PD2530-HeatsinkAssembly.md
[PD2530-HeatsinkAssemblyPCBA]: ../pcbs/pd2530-heatsink-assy
