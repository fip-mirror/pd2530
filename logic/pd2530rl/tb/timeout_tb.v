module timeout_tb();

reg  clk, signal, clear, timer;
wire timedout;

timeout timeout(
	.clk(clk),
	.signal(signal),
	.clear(clear),
	.timer(timer),
	.timedout(timedout)
);

initial begin
	$dumpfile("out.vcd");
	$dumpvars(0,timeout_tb);

	// Feed in a signal and make sure there is no timeout
		clk = 1'b0; signal = 1'b1; clear = 1'b0; timer = 1'b0;
	#1	clk = 1'b1;
	#1	assert(~timedout);

	// Remove the signal, there still shouldn't be a timeout
	#1	clk = 1'b0; signal = 1'b0;
	#1	clk = 1'b1;
	#1	assert(~timedout);

	// Give one timer tick, this will clear saw_signal
	#1	clk = 1'b0; timer = 1'b1;
	#1	clk = 1'b1;
	#1	assert(~timedout);

	// Give the second timer tick, now it should.
	#1	clk = 1'b0; timer = 1'b1;
	#1	clk = 1'b1;
	#1	assert(timedout);

	// Bring the signal back in, we shouldn't clear yet
	#1	clk = 1'b0; timer = 1'b0; signal = 1'b1;
	#1	clk = 1'b1;
	#1	assert(timedout);

	// Now clear
	#1	clk = 1'b0; timer = 1'b0; clear = 1'b1;
	#1	clk = 1'b1;
	#1	assert(~timedout);

	// Remove the signal and time out one more time
	#1	clk = 1'b0; signal = 1'b0; clear = 1'b0; timer = 1'b1;
	#1	clk = 1'b1;
	#1	clk = 1'b0;
	#1	clk = 1'b1;
	#1	assert(timedout);

	// Bring in signal and clear simultaneously to simulate them being
	// tied together
	#1	clk = 1'b0; signal = 1'b1; clear = 1'b1; timer = 1'b0;
	#1	clk = 1'b1;
	#1	assert(~timedout);
end

endmodule
