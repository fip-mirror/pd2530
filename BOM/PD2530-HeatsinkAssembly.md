# BOM: PD2530-HeatsinkAssembly

## Parts

| Qty | Item | Manufacturer/Distributor | Part number | Substitutions? |
|----:|------|--------------|-------------|----------------|
|   1 | 100x69x36mm extruded aluminum heatsink | Amazon | B07DH5T2RW | Y |
|   2 | 100x25x10mm extruded aluminum heatsink | Amazon | B00UJ95TOE | Y |
|   8 | High-performance TO-220 thermal pad | t-Global Technology | DC0011/05-TG-A482K-0.1-0 | N |
|   1 | [PD2530-HeatsinkAssemblyPCBA]       | Custom              | N/A | N |
|   2 | Mounting bracket                    | Keystone            | 4337 | Y |
|   2 | M4x10 Torx-drive zinc-plated screws | McMaster-Carr       | 90304A216 | Y (ensure zinc or aluminum!) |
|   6 | M3x20 Torx-drive zinc-plated screws | McMaster-Carr       | 90304A337 | Y (ensure zinc or aluminum!) |
|   1 | M3x6 Torx-drive zinc-plated screws  | McMaster-Carr       | 90304A211 | Y (ensure zinc or aluminum!) |
|   6 | #6 oversized (0.625in) washers      | McMaster-Carr       | 91090A100 | Y (ensure zinc or aluminum!) |
|     | Medium-strength threadlocker        | Loctite             | 242 | Y |

## Assembly

**Warning:** hardware for direct use in the heatsink assembly should be
zinc-plated or aluminum. Brass and stainless steel screws will cause corrosion
when installed into aluminum. To keep these screws separate, the heatsink
assembly uses Torx-drive screws.

1. Drill holes in heatsinks as specified in the diagram below.
2. Tap holes as specified.
3. Place the PCBA onto the large heatsink, and gently hold it down with the
   top-center M3x6 screw as indicated with an arrow on the silkscreen.
4. Install the mounting brackets and screw them down gently using temporary
   threadlocker on the M4x10 screws.
5. Tighten all three screws snugly, being careful to keep the bottom surfaces
   of the mounting brackets flush with the bottom surface of the large heatsink.
6. Gently lift the TO-220 transistors forward, slide the thermal pads underneath
   them and press them back down. Ensure the corners of the transistors do not
   cut through the thermal pads.
7. Place the upper small heatsink across the top row of transistors, and screw
   it through to the large heatsink using 3 of the #6 washers and 3 of the
   M3x20 screws.
8. Repeat the above step with the lower small heatsink.

Mechanical drawing: [pd2530-heatsinks.pdf]

## Exploded-view animation

An animation showing the assembly and disassembly can be found here:
https://diode.zone/w/n82ke9wHq891P6FsZd7pPB

This was created from the mechanical model at
[../mech/HeatsinkAssyExploded.FCStd](../mech/HeatsinkAssyExploded.FCStd).

[PD2530-HeatsinkAssembly]: PD2530-HeatsinkAssembly.md
[PD2530-HeatsinkAssemblyPCBA]: ../pcbs/pd2530-heatsink-assy
[pd2530-heatsinks.pdf]: ../drawings/pd2530-heatsinks/pd2530-heatsinks.pdf
