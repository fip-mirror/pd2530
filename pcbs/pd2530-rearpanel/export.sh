#!/bin/sh

kicad_export -O export --clear \
	--gerbers --gerb-n-layers=2 --gerb-pack --gerb-render \
	pd2530-rearpanel
