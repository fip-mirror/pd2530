module oscsync(
	input presync_cmd,
	input osc,
	input sync_to_osc,

	output supply_cmd,

	input clk
);

reg sync_supply_cmd = 1'b0;
reg last_osc = 1'b0;

always @(posedge clk) begin
	last_osc <= osc;

	if (~presync_cmd) begin
		sync_supply_cmd <= 1'b0;
	end else if (~osc & last_osc) begin
		sync_supply_cmd <= presync_cmd;
	end
end

assign supply_cmd = sync_to_osc ? sync_supply_cmd : presync_cmd;

endmodule
